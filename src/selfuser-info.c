#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../include/utils.h"
#include "../include/urls.h"
#include "../include/selfuser-info.h"

char *get_csrftoken(CURL *curl)
{
	char toFind[] = "set-cookie: csrftoken=";
	struct headers_t header = {NULL, 0};
	char *result = NULL;

	curl_easy_reset(curl);
	get_request(
		curl, OSTGET_LANDING_INFO,
	    get_header_callback, blank_write_callback,
	    &header, NULL, NULL
	);

	// Search the specific header to found and
	// extract and save it corresponding value
	for (int x = 0; x < header.length - 1; x++) {
		int founded = 0;

		// Compare letters
		for (int y = 0; y < (sizeof(toFind) - 2); y++) {
			if (header.headers[x][y] != toFind[y]) {
				founded = 0;
				break;
			} else founded = 1;
		}

		// If founded == 1 extract the csrftoken value
		// and save in the return pointer
		if (founded) {
			int index = sizeof(toFind) - 1;
			int mul = 1;
			char c = ' ';

			while (founded) {
				c = header.headers[x][index++];

				result = realloc(result, sizeof(char) * mul);

				if (c == ';') {
					result[mul-1] = '\0';
					break;
				} else result[mul-1] = c;
				mul++;
			}

			break;
		}
	}

	return result;
}

int get_login_user_info(char *userAgent, char *csrf,
						char **dsUserId, char **sessionId,
						char *username, char *password,
						CURL *curl)
{
	char *tmpUserAgent = join_str("User-Agent: ", userAgent);
	char *tmpCsrf = join_str("X-CSRFToken: ", csrf);

	char template1[] = "enc_password=#PWD_INSTAGRAM_BROWSER:0:1675237980:"; 
	char template2[] = "&queryParams={}&optIntoOneTap=false&trustedDeviceRecords={}";

	int dataLength = strlen(username) + strlen(password) +
					 strlen(template1) + strlen(template2) + 10;
	char *data = malloc(dataLength);
	data[0] = '\0';
	strcat(data, template1);
	strcat(data, password);
	strcat(data, "&username=");
	strcat(data, username);
	strcat(data, template2);
	data[dataLength] = '\0';

	struct curl_slist *list = NULL;
	list = curl_slist_append(list, tmpUserAgent);
	list = curl_slist_append(list, tmpCsrf);

	struct headers_t header = {NULL,0};
	curl_easy_reset(curl);
	long statusCode = post_request(curl, OSTPOST_LOGIN,
								   get_header_callback, blank_write_callback,
								   &header, list, data, dataLength);
	free(data); 		data = NULL;
	free(tmpUserAgent); tmpUserAgent = NULL;
	free(tmpCsrf); 		tmpCsrf = NULL;

	if (statusCode != 200) {
		printf("%s\n", "LOG: Error doing the request [get_login_user_info]");
		return 0;
	}

	unsigned short flag1 = 0;
	unsigned short flag2 = 0;
	for (int x = 0; x < header.length; x++) {
		if (strstr(header.headers[x], "set-cookie: ds_user_id") != NULL) {
			char *str = header.headers[x];
			*dsUserId = get_cookie_value(str, "ds_user_id");
			if (*dsUserId == NULL) return 0;
			flag1++;
		}

		if (strstr(header.headers[x], "set-cookie: sessionid") != NULL) {
			char *str = header.headers[x];
			*sessionId = get_cookie_value(str, "sessionid");
			if (*sessionId == NULL) return 0;
			flag2++;
		}

		if (flag1 && flag2) break;
	}

	return 1;
}

// Private function for free values of the following function
void __free_values(char *csfr, char *dsUserId, char *sessionId)
{
	free(csfr); 		csfr = NULL;
	free(dsUserId); 	dsUserId = NULL;
	free(sessionId); 	sessionId = NULL;
}

int gen_basic_userinfo_file_config(char *route, char *username,
								   char *password, CURL *curl)
{
	char userAgent[] = "Mozilla/5.0 "
					   "(X11; Linux x86_64; rv:109.0) "
					   "Gecko/20100101 Firefox/109.0";
	char appId[] = "936619743392459";
	char *rawCsfrToken = get_csrftoken(curl);
	char *dsUserId = NULL;
	char *sessionId = NULL;
	
	get_login_user_info(userAgent, rawCsfrToken, &dsUserId,
						&sessionId, username, password, curl);

	char *headers[] = {"User-Agent", "X-IG-App-ID",
					  "raw_csrf_token", "csrftoken",
					  "X-CSRFToken", "ds_user_id",
					  "sessionid"};
	char *values[] = {userAgent, appId, rawCsfrToken,
					 rawCsfrToken, rawCsfrToken, dsUserId,
					 sessionId};


	FILE *fd = fopen(route, "w");

	// Treaty of errors
	if (fd == NULL) {
		printf("%s\n", "LOG: Error creating the file");
		__free_values(rawCsfrToken, dsUserId, sessionId);
		fclose(fd);
		return 0;
	}

	if (!(rawCsfrToken && dsUserId && sessionId)) {
		printf("%s\n", "LOG: Error getting basics informations");
		__free_values(rawCsfrToken, dsUserId, sessionId);
		return 0;
	}

	// Join and save the headers to a config file
	for (int x = 0; x < sizeof(values)/sizeof(char *); x++) {
		size_t result = 0;
		
		if (x >= (sizeof(values)/sizeof(char *)) - 2) {
			// When are the last two fields change to cookie format
			result = fprintf(fd, "%s=%s;\n", headers[x], values[x]);
		} else {
			// Else print as header format
			result = fprintf(fd, "%s: %s\n", headers[x], values[x]);
		}

		if (result != strlen(headers[x]) + strlen(values[x]) + 3) {
			printf("%s\n", "LOG: Error data not written correctly");
			__free_values(rawCsfrToken, dsUserId, sessionId);
			fclose(fd);
			return 0;
		}
	}

	__free_values(rawCsfrToken, dsUserId, sessionId);
	fclose(fd);
	return 1;
}

int get_userinfo_from_file_config(char *route, struct userinfo_t *userinfo)
{
	FILE *fd = fopen(route, "r");
	const int bufferSize = 200;
	char buffer[bufferSize];

	// Treaty of errors
	if (fd == NULL) {
		fprintf(stderr, "%s\n", "LOG: Error config file not founded");
		fclose(fd);
		return 0;
	}

	int count = 0;
	char c;
	int line = 0;
	while (feof(fd) == 0) {
		c = fgetc(fd);

		if (c == EOF) break;

		if (count == bufferSize) {
			fprintf(stderr,
					"LOG: Warning, config file field exceed"
					" the characters buffer of %d bytes\n\n",
					bufferSize);
			return 0;
		}

		if (line == USERINFO_NUMBER_OF_FIELDS-1) {
			fprintf(stderr,
					"LOG: ERROR, Number of fields in"
					" the file are more than the required by"
					" the userinfo_t structure\n\n");
			return 0;
		}

		if (c == '\n') {
			buffer[count] = '\0';
			char *field = malloc(sizeof(char) * strlen(buffer) + 1);
			strcpy(field, buffer);
			*((void **)&(*userinfo)+line++) = field;
			count = 0;
			continue;
		} else {
			buffer[count] = c;
			count++;
		}

	}

	for (line = 0; line < USERINFO_NUMBER_OF_FIELDS; line++) {
		if (*((void **)&(*userinfo)+line++) == NULL) {
			fprintf(stderr,
					"LOG: WARNING, Number of fields in"
					" the file are less than the required by"
					" the userinfo_t structure\n\n");
			return 0;
		}
	}

	fclose(fd);
	return 1;
}

