#include <stdio.h>
#include <curl/curl.h>
#include <string.h>

#include "./include/utils.test.h"
#include "../include/utils.h"

// Test if join two strings correctly
int join_str_test()
{
	int result = strcmp(join_str("Hello", ", World!"), "Hello, World!");
	return result == 0 ? 1 : 0;
}

// Test if get correctly the value of the cookie searched from a cookie
// http header
int get_cookie_value_test()
{
	char cookieString[] = "cookie1=value1098; cookie2=value2083; cookie3=test;";
	int result = 0;
	result += strcmp(get_cookie_value(cookieString, "cookie1"), "value1098");
	result += strcmp(get_cookie_value(cookieString, "cookie2"), "value2083");
	result += strcmp(get_cookie_value(cookieString, "cookie3"), "test");
	return result == 0 ? 1 : 0;
}

// Test if can get correctly a option from the user.
// Test if only the first digit of a valid number is correctly a valid 
// digit number and only one digit taken into account.
// Just numbers can be accepted
int get_option_test()
{
	printf("\n*** Write a number, only the first digit will "
			"be taken into account ***\n");
	const int result = get_option(0, 9);
	return result >= 0 && result <= 9 ? 1 : 0;
}

int input_test()
{
	const int bufferSize = 19;
	char value[bufferSize];
	input("\nEnter 'Hello, World!123.' as an input >> ", value, bufferSize);
	return strcmp(value, "Hello, World!123.") == 0 ? 1 : 0;
}
