#ifndef UTILS_H_
#define UTILS_H_

/* Headers Struct.
 * 
 * char -> list of strings of headers.
 * int  -> number of lines, one per header.
 * */
struct headers_t {
	char **headers;
	int length;
};

/* Enum of help pages and sub-pages in existence. */
enum HELP_PAGE {
	HLP_GENERAL,
	HLP_AUTHENTICATION
};

/* CALLBACKS FUNCTIONS */

/* No write on the standard output the body of the request. */
size_t blank_write_callback(char *ptr, size_t size,
							size_t nmemb, void *userdata);

size_t write_write_callback(char *ptr, size_t size, size_t nmemb, void *userdata);

/* Save the data on a buffer */
size_t get_write_callback(char *ptr, size_t size, size_t nmemb, void *userdata);

/* No write on the a buffer header the header of the request. */
size_t blank_header_callback(char *buffer, size_t size,
							 size_t nitems, void *userdata);

size_t write_header_callback(char *buffer, size_t size, size_t nitems, void *userdata);

/* Write on the a buffer header the header of the request. */
size_t get_header_callback(char *buffer, size_t size,
						   size_t nitems, void *userdata);

/* Execute and get the header and body of a get request.
 * 
 * long -> request status code
 * ---
 * CURL * <- curl pointer.
 * char * <- url target to do the get request.
 * size_t <- header_callback_function. Libc documentation.
 * size_t <- writer_callback_function. Libc documentation.
 * struct headers_t <- header struct pointer.
 * */
long get_request (
	CURL *curl, char *url,
	size_t (*hf) (char *,size_t,size_t,void *),
	size_t (*wf) (char *,size_t,size_t,void *),
	struct headers_t *header, char **data, struct curl_slist *list
);

/* Execute and get the header and body of a post request.
 * 
 * long -> request status code
 * ---
 * CURL * <- curl pointer.
 * char * <- url target to do the get request.
 * size_t <- header_callback_function. Libc documentation.
 * size_t <- writer_callback_function. Libc documentation.
 * struct headers_t <- header struct pointer.
 * struct curl_slist <- list of headers.
 * char * <- data body.
 * long <- data body size.
 * */
long post_request(CURL *curl, char *url,
				size_t (*hf) (char *,size_t,size_t,void *),
				size_t (*wf) (char *,size_t,size_t,void *),
				struct headers_t *header, struct curl_slist *list,
				char *data, long dataSize);

/* Join two strings of characters.
 *
 * char -> Pointer to the new joined str. Need to be freed
 * ---
 * char <- First string to be joined.
 * char <- Second string to be joined.
 * */
char *join_str(char *str1, char *str2);

/* Get cookie value from the Cookie section in the header.
 *
 * char * -> Value of the cookie between '=' and ';' chars.
			 Return NULL if not found. Need to be freed.
 * ---
 * char * <- Str where find the cookie value.
 * char * <- Str of the cookie to be searched.
 * */
char *get_cookie_value(char *cookieString, char *cookieName);

/* Read a .txt help page and print to the stdin.
 *
 * int <- Enum HELP_PAGE.
 * */
void print_help(int helpId);

/* Ask input, get first char, validate if is between r1 and r2
 * and return the number if it's between.
 *
 * Note: Clear stdin buffer.
 *
 * int -> Selected valid option.
 * ---
 * int <- Range init.
 * int <- Range end.
 * */
int get_option(int r1, int r2);

/* Print label to stdout, wait and analyze the user input
 * and save it in *value.
 *
 * Note: Print error and exit program when bufferSize
 * limits are exceeded.
 *
 * char <- Label string to print.
 * char <- Memory space where input will be saved.
 * int  <- *value memory space size.
 * */
void input(char *label, char *value, int bufferSize);

#endif // UTILS_H_
