#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../include/utils.h"
#include "../include/selfuser-info.h"
#include "../include/screens.h"

void main_screen(struct userinfo_t *userinfo)
{
	while (1) {
		system("clear");
		printf("Main Menu\n\n");
		printf("1. Authenticate\n");
		printf("2. Exit\n");
		printf("\n");

		int selection = get_option(1, 2);

		if (selection == 1) {
			system("clear");
			login_screen(NULL, NULL, userinfo);
		} else if (selection == 2) break;
	}
}

int login_screen(char *credentials, char *path, struct userinfo_t *userinfo)
{
 	char username[320];
 	char password[320];
 	char defaultPath[] = "./userinfo.conf";
	char lpath[4096];
	CURL *curl = curl_easy_init();

	// Enter when is command line call
	if (credentials != NULL) {
		int change = 0;
		int index = 0;

		// Map format user:pass to variables
		for (int x = 0; credentials[x] != '\0'; x++) {
			if (!change) {
				if (credentials[x] == ':') {
					username[x] = '\0';
					change++;
				} else username[x] = credentials[x];
			} else {
				password[index++] = credentials[x];
			}
		}

		// Return error if lack of information in credentials
		if (username[0] == '\0' || password[0] == '\0') {
			curl_easy_cleanup(curl);
			return 0;
		}

		printf("Trying to login, wait...\n");
		int r = 0;
		if (path == NULL) {
			r = gen_basic_userinfo_file_config(defaultPath, username, password,
											   curl);
		} else {
			r = gen_basic_userinfo_file_config(path, username, password, curl);
		}

		if (!r) {
			fprintf(stderr, "LOG: Error, failed to login\n");
			fprintf(stderr, "LOG: Terminating program\n");
			return 0;
		}

		printf("LOG: Login and file generation completed successfully.\n");
		curl_easy_cleanup(curl);
		return 1;
	}

	// Enter when is in iterative menu mode
	int selection = 0;

	printf("Login Menu\n\n");
	printf("1. Login/generate userinfo.conf\n");
	printf("2. Login with userinfo.conf file\n");
	printf("3. Back\n");
	printf("4. Exit\n");
	printf("\n");

	selection = get_option(1, 4);

	if (selection == 1) {
		input("Enter username/email [MAX 320]\n>> ",
			   username, 320);
		input("Enter password [MAX 320]:\n>> ",
			   password, 320);
		printf("\n");
		input("Path where to save userinfo.conf"
			   "[default ./userinfo.conf]\n>> ", lpath, 4096);

		if (lpath[0] == '\0')
			strcpy(lpath, defaultPath);

		int r = gen_basic_userinfo_file_config(lpath, username, password, curl);
		int r2 = get_userinfo_from_file_config(lpath, userinfo);
		if (!r || !r2) {
			fprintf(stderr, "LOG: Error, failed to login\n");
			fprintf(stderr, "LOG: Terminating program\n");
			return 0;
		}

		printf("LOG: Login and file generation completed successfully.\n\n");
		printf("Press any key to continue...\n");
		getchar();
		curl_easy_cleanup(curl);
	} else if (selection == 2) {
		input("Path where is saved the userinfo.conf"
			   "[default ./userinfo.conf]\n>> ", lpath, 4096);

		if (lpath[0] == '\0')
			strcpy(lpath, defaultPath);

		printf("\nLOG: Retrieving the information, wait...\n");
		int r = get_userinfo_from_file_config(lpath, userinfo);

		if (!r) {
			printf("LOG: Error retrieving the information.\n");
			return 0;
		} else {
			printf("LOG: Information retrieved successfully.\n\n");
			printf("Press any key to continue...\n");
			getchar();
		}
	} else if (selection == 3) {
		system("clear");
		return 1;
	} else if (selection == 4) exit(0);

	return 1;
}

