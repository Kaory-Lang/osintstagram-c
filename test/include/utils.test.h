#ifndef UTILS_TEST_H_
#define UTILS_TEST_H_

int join_str_test();
int get_cookie_value_test();
int get_option_test();
int input_test();

#endif // UTILS_TEST_H_
