#ifndef SCREENS_H_
#define SCREENS_H_

/* Main screen logic.
 *
 * struct userinfo_t <- Pointer to static self user info.
 * */
void main_screen(struct userinfo_t *userinfo);

/* Login screen and one line command logic.
 *
 * int ->  1 if all OK. 0 when erros occurs.
 * ---
 * char <- Credentials in the formar username:password.
 * 		   NULL if is in one line command mode.
 * char <- Path where save userinfo.conf file.
 * 		   NULL if is in one line command mode.
 * struct userinfo_t <- Pointer to static self user info.
 * */
int login_screen(char *credentials, char *path, struct userinfo_t *userinfo);

#endif // SCREENS_H_
