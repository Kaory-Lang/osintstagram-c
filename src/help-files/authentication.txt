Multitool of OSINT techniques to the Instagram platform.

Mode: Authenticate as a user.

Usage: 	osinstagram -A [username|email:password] [...OPTIONS] [...VALUES]

Options:
	Those that have the '*' symbol have a sub-help display.
	Access with the flag selected plus [-h|--help] flags.

	-o, --output 	 	where to save the generated userinfo.conf file.
						By default will be "./userinfo.conf".

	-h, --help 			Show command line help.

Examples:
	osinstagram -A admin:adminpass -o "./userinfo.conf
	osinstagram -A admin@email.com:adminpass
