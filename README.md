# Windows Build From Source Guide

### Requirements

- Mingw (will give us the compiler clang)
- libcurl (include directory and libcurl.dll)

### step #1 - Download llvm-mingw

Download llvm-mingw from the [official page](https://github.com/mstorsjo/llvm-mingw/releases)

### step #2 - Add the mingw bin path to environment PATH vairable.

Go to System Properties -> Environment Variables -> Edit Path and add path to the mingw bin directory.

Test if it work opening a CMD and typing ```clang --version``` if all correct need to print some info about clang.

### step #3 - Download curl project for windows

Go to [curl download page](https://curl.se/download.html) and select the version of curl.

In this case will be Windows 64-bit.

Download, extract it and move all from include directory (curl\include\\*) to the mingw\include directory.

Finally move (curl\bin\libcurl-x64.dll) in this case the x64 version to the mingw\lib directory.

### step #4 - compile

Finally execute the windows batch file from the osinitstagram project.

This generate a .exe file, copy the libcurl-x64.dll to the same directory where is the executable or to the windows\system32 for a global utilization.

