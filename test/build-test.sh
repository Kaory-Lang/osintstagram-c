#!/bin/bash

clang \
	./*.c ../src/utils.c ../src/selfuser-info.c ../src/features.c \
	-lcurl -ljson-c \
	-o run-tests \
	-Wall \
	-Wno-unused-variable
