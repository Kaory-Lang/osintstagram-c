#ifndef SELFUSER_INFO_H_
#define SELFUSER_INFO_H_

/* Self user info struct.
 * 
 * Keeps all the requiered informations for
 * do the requests. All already formated
 * as cookies.
 *
 * If length change update the value of the macro below.
 * */
struct userinfo_t {
	char *userAgent;
	char *appId;
	char *csrfTokenRaw;
	char *csrfTokenCookie;
	char *csrfTokenX;
	char *dsUserId;
	char *sessoinId;
};
#define USERINFO_NUMBER_OF_FIELDS 8

/* Get csrftoken. Initial step to login in the page
 *
 * char * -> NULL if could't get it. Need to be freed.
 * ---
 * CURL * <- curl pointer.
 * */
char *get_csrftoken(CURL *curl);

/* Get required informations about the selfuser.
 * As the ds_user_id and the sessionid.
 *
 * int -> 1 if all ok. 0 when error occurs.
 * ---
 * char * <- Str of the User-Agent.
 * char * <- Login csrf token.
 * char * <- Pointer to the ds_user_if string to fill.
 * char * <- Pointer to the sessionid string to fill.
 * char * <- Username/email string.
 * char * <- Password string.
 * CURL * <- Curl pointer.
 * */
int get_login_user_info(char *userAgent, char *csrf,
						char **dsUserId, char **sessionId,
						char *username, char *password,
						CURL *curl);

/* Generate a config file with all the requiered 
 * userinfo_t informations dump to the storage.
 *
 * int -> 1 if all ok. 0 when error occurs.
 * ---
 * char * <- Str with the path where the file will be saved.
 * char * <- Username/email string.
 * char * <- Password string.
 * CURL * <- Curl pointer.
 * */
int gen_basic_userinfo_file_config(char *route, char *username,
								   char *password, CURL *curl);

/* Fill a userinfo_t struct with the data of in a userinfo
 * configuration file.
 *
 * int -> 1 if all ok. 0 when error occurs.
 * ---
 * char * <- Str with the path where the file is saved.
 * struct userinfo_t * <- Pointer to the userinfo_t struct to fill.
 * */
int get_userinfo_from_file_config(char *route, struct userinfo_t *userinfo);

#endif // SELFUSER_INFO_H_

