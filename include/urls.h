/* Define all the urls that can be used */

#define TEST_URL "https://www.google.com/"
#define OSTGET_LANDING_INFO "https://www.instagram.com/api/v1/public/landing_info/"
#define OSTPOST_LOGIN "https://www.instagram.com/api/v1/web/accounts/login/ajax/"
#define OSTGET_USERFROMID "https://i.instagram.com/api/v1/users/"
