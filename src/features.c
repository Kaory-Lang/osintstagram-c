#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <json-c/json.h>
#include <string.h>

#include "../include/features.h"
#include "../include/utils.h"
#include "../include/selfuser-info.h"
#include "../include/urls.h"
#include "../include/user-agents.h"

char *user_from_id(char *id, CURL *curl, struct userinfo_t *userinfo)
{
	if (id == NULL || curl == NULL || userinfo == NULL) return NULL;

	struct headers_t header = {NULL, 0};
	char *data = NULL;

	char *join1 = join_str(OSTGET_USERFROMID, id);
	char *url = join_str(join1, "/info/");
	char *selfUserIdCookie = join_str("Cookie: ", userinfo->dsUserId);

	struct curl_slist *list = NULL;
	list = curl_slist_append(list, OSTUG_ANDROID_APP);
	list = curl_slist_append(list, selfUserIdCookie);
	long r =  get_request(
		curl, url, blank_header_callback, get_write_callback,
		&header, &data, list
	);

	// Free memory
	free(join1); join1 = NULL;
	free(url); url = NULL;
	free(selfUserIdCookie); selfUserIdCookie = NULL;

	// If fail doing the request return NULL
	if (r != 200) {
		free(data); data = NULL;
		return NULL;
	}

	// Parsing the json to string
	struct json_object *json = json_tokener_parse(data);
	struct json_object *user;
	struct json_object *username;
	json_object_object_get_ex(json, "user", &user);
	json_object_object_get_ex(user, "username", &username);
	free(data); data = NULL;

	const char* usernameStr = json_object_get_string(username);
	if (usernameStr == NULL) return NULL;
	char *result = malloc(sizeof(char) * strlen(usernameStr));
	strcpy(result, usernameStr);
	while (json_object_put(json) != 0); 	// Free memory of the json_object

	return result;
}
