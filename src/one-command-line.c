#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../include/utils.h"
#include "../include/selfuser-info.h"
#include "../include/screens.h"
#include "../include/one-command-line.h"

// Private enum, flags ids in existence
enum FLAG_ID {
	FLAG_NON_EXIST = -1,
	FLAG_HELP_ID,
	FLAG_AUTHENTICATE_ID,
	FLAG_OUTPUT_ID
};

// Private function, analyze argument and return
// flag id when match with one.
// Else return FLAG_NON_EXIST (-1)
int __flag_id(char *arg)
{
	const int FLAGS_N = 3;
	char flags[3][2][50] = {
		{"-h", "--help"},
		{"-A", "--authenticate"},
		{"-o", "--output"}
	};

	for (int x = 0; x < FLAGS_N; x++) {
		int cmp1 = strcmp(flags[x][0], arg) == 0;
		int cmp2 = strcmp(flags[x][1], arg) == 0;

		if (cmp1 || cmp2) return x;
	}

	return -1;
}

int one_command_action(int argc, char **argv)
{
	int position = 1;
	int id = __flag_id(argv[position]);
	
	if (id == FLAG_HELP_ID) {
		print_help(HLP_GENERAL);
		return 1;
	} else if (id == FLAG_AUTHENTICATE_ID) {
		char *credentials = NULL;
		char *path = NULL;
	
		for (int x = position + 1; x < argc; x++) {
			if (__flag_id(argv[x]) == FLAG_NON_EXIST) {
				credentials = argv[x];
			} else if (__flag_id(argv[x]) == FLAG_HELP_ID) {
				print_help(HLP_AUTHENTICATION);
				return 1;
			} else if (__flag_id(argv[x]) == FLAG_OUTPUT_ID) {
				if ((x + 1) < argc) {
					path = argv[x+1];
					break;
				}
			}
		}
	
		if (credentials == NULL) return 0;

		if(login_screen(credentials, path, NULL))
			return 1;
	}

	return 0;
}

