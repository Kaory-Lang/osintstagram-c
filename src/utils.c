#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../include/utils.h"
#include "../include/urls.h"

struct headers_t *tmphd;
char **tmpData;

size_t blank_write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	return size * nmemb;
}

size_t write_write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	printf("%s\n", ptr);
	return size * nmemb;
}

size_t get_write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	*tmpData = malloc((sizeof(char) * nmemb));
	strcpy(*tmpData, ptr);
	return size * nmemb;
}

size_t blank_header_callback(char *buffer, size_t size, size_t nitems, void *userdata)
{
	return nitems * size;
}

size_t write_header_callback(char *buffer, size_t size, size_t nitems, void *userdata)
{
	printf("%s\n", buffer);
	return nitems * size;
}

size_t get_header_callback(char *buffer, size_t size, size_t nitems, void *userdata)
{
	size_t chunkSize = nitems * size;

	// Jump the white space of the header
	if (buffer[0] == '\r') return nitems * size;

	// Expand the header list
	tmphd->headers = realloc(tmphd->headers, sizeof(char *) * tmphd->length + 1);

	// Erease \r\n header characters from original buffer
	buffer[(nitems * size) - 2] = '\0';

	// Allocate a new pointer and copy the edited buffer there
	char *ptr = malloc((chunkSize) - 2);
	strncpy(ptr, buffer, chunkSize - 2);
	tmphd->headers[tmphd->length++] = ptr;

	// Validate the amount data is correct
	return chunkSize;
}

long get_request(
	CURL *curl, char *url,
	size_t (*hf) (char *,size_t,size_t,void *),
	size_t (*wf) (char *,size_t,size_t,void *),
	struct headers_t *header, char **data, struct curl_slist *list
)
{
	tmphd = header;
	tmpData = data;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wf);
	curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, hf);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

	curl_easy_perform(curl);

	long http_code = 0;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

	return http_code;
}

long post_request(CURL *curl, char *url,
				size_t (*hf) (char *,size_t,size_t,void *),
				size_t (*wf) (char *,size_t,size_t,void *),
				struct headers_t *header, struct curl_slist *list,
				char *data, long dataSize)
{
	tmphd = header;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wf);
	curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, hf);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
	curl_easy_setopt(curl, CURLOPT_POST, 1L);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, dataSize);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

	curl_easy_perform(curl);

	long http_code = 0;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

	return http_code;
}

char *join_str(char *str1, char *str2)
{
	char *newStr = malloc(sizeof(char) * (strlen(str1) + strlen(str2) + 1));
	newStr[0] = '\0';
	strcat(newStr, str1);
	strcat(newStr, str2);
	return newStr;
}

char *get_cookie_value(char *cookieString, char *cookieName)
{
	int init = strlen(cookieName) + 1;
	int final = 0;
	char *result = NULL;

	char *str = strstr(cookieString, cookieName);

	if (str == NULL) {
		printf("%s\n", "LOG: Cookie value not obtained [get_cookie_value]");
		return result;
	}

	// Get the cookie value between '=' and ';'
	for (int x = 0; x < strlen(str); x++) {
		if (str[x] == ';') {
			final = x;
			break;
		}
	}

	int length = final - init;
	result = malloc(sizeof(char) * length);

	for (int x = init; x < (init + length); x++)
		result[x - init] = str[x];

	result[length] = '\0';

	return result;
}

void print_help(int helpId)
{
	char *route = NULL;

	if (helpId == HLP_GENERAL)
		route = "./src/help-files/general.txt";
	if (helpId == HLP_AUTHENTICATION)
		route = "./src/help-files/authentication.txt";

	if (route == NULL) {
		fprintf(stderr, "LOG: ERROR, wrong help file path");
		return;
	}

	FILE *fd = fopen(route, "r");
	char c;
	while (feof(fd) == 0) {
		if ((c = fgetc(fd)) != EOF)
			printf("%c", c);
	}
	printf("\n");
	fclose(fd);
}

int get_option(int r1, int r2)
{
	int selection = -1;

	while (!(selection >= r1 && selection <= r2)) {
		printf("\033[2K"); 	// Erease entire line
		printf("Select a valid option: ");
		selection = getchar() - 48;
		printf("\033[F"); 	// \n opositive
		while (getchar() != '\n'); 	// Clear buffer. Just take first char.
	}

	printf("\n\n");
	return selection;
}

void input(char *label, char *value, int bufferSize)
{
	printf("%s", label);
	int x = 0;
	char c;
	while ((c = getchar()) != '\n') {
		if (x < bufferSize)
			value[x++] = c;
		else {
			fprintf(stderr, "LOG: ERROR, buffer limit exceded.\n");
			printf("LOG: Program terminated...\n");
			exit(1);
		}
	}

	value[x] = '\0';
}

