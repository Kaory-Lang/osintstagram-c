/* Some functions are omitted because it's difficult validate the
 * outputs or are unnecessary because other more important
 * functions depend on them and those are validated all at the same time. 
 * */

#include <stdio.h>
#include <curl/curl.h>

#include "./include/utils.test.h"
#include "./include/selfuser-info.test.h"
#include "./include/features.test.h"

#define ACTIVATE 1 // activate or deactivate all tests. Customized for each too

int testNumber = 0;
int testsPassed = 0;
int testsFailed = 0;
int totalFunctions = 0;

int self_user_tests_passed = 0;

void reset_variables()
{
	testNumber = 0;
	testsPassed = 0;
	testsFailed = 0;
	totalFunctions = 0;
}

void test(int passed, char *testName)
{
	printf("%s - Test #%d [%s]\n",
			passed ? "Passed" : "Failed", testNumber, testName);
	if (passed) testsPassed++; else testsFailed++;
	testNumber++;
}

void final_message()
{
	printf("\nResult: (total_function: %d, omitted: %d, "
			"tested: %d, passed: %d, failed: %d)\n",
			totalFunctions, totalFunctions - testNumber,
			testNumber, testsPassed, testsFailed);
	reset_variables();
}

int main(int argc, char **argv)
{
#if ACTIVATE 
	printf("---- utils.c tests ----\n\n");
	totalFunctions = 12;
	test(join_str_test(), "join_str_test");
	test(get_cookie_value_test(), "get_cookie_value_test");
	test(get_option_test(), "get_option_test");
	test(input_test(), "get_input_test");
	final_message();
#endif

#if ACTIVATE // Required that all pass sussccefully for the following tests
	printf("\n---- self-user.c tests ----\n\n");
	totalFunctions = 4;
	test(get_userinfo_from_file_config_test(), "get_userinfo_from_conf_file");
	test(gen_basic_userinfo_file_config_test(), "gen_basic_userinfo_conf_file");
	if (testsFailed == 0) self_user_tests_passed = 1;
	final_message();
#endif

#if ACTIVATE 
	printf("\n---- features.c tests ----\n\n");
	totalFunctions = 1;
	if (self_user_tests_passed) {
		test(user_from_id_test(), "get_user_from_id");
	}
	final_message();
#endif

	return 0;
}

