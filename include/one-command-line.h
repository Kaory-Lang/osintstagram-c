#ifndef ONE_COMMAND_LINE_H_
#define ONE_COMMAND_LINE_H_

/* In CLI one line command analyze args,
 * detects flags and sub-flags to complete an
 * action.
 *
 * int -> Status code. 1 if all OK. 0 When errors occur.
 * ---
 * int     -> Arguments count [argc of the main function].
 * char ** -> Arguments list [argv of the main function].
 * */
int one_command_action(int argc, char **argv);

#endif // ONE_COMMAND_LINE_H_
