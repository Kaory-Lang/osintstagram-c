#include <stdio.h>
#include <curl/curl.h>
#include <string.h>

#include "./include/selfuser-info.test.h"
#include "../include/selfuser-info.h"
#include "../include/utils.h"

// Test if read correctly a config file with the selfuser information
int get_userinfo_from_file_config_test()
{
	struct userinfo_t userinfo;
	if (get_userinfo_from_file_config("./test.conf", &userinfo)) {
		int result = 0;
		result += strcmp(userinfo.userAgent, "User-Agent: value1");
		result += strcmp(userinfo.appId, "X-IG-App-ID: value2");
		result += strcmp(userinfo.csrfTokenRaw, "raw_csrf_token: value3");
		result += strcmp(userinfo.csrfTokenCookie, "csrftoken: value4");
		result += strcmp(userinfo.csrfTokenX, "X-CSRFToken: value5");
		result += strcmp(userinfo.dsUserId, "ds_user_id=value6;");
		result += strcmp(userinfo.sessoinId, "sessionid=value7;");

		return result == 0 ? 1 : 0;
	} else return 0;
}

// Test if login to the platform correctly and save all the necesary
// user information for the next requests in a userinfo.conf file.
int gen_basic_userinfo_file_config_test()
{
	CURL *curl = curl_easy_init();
	char username[320];
	char password[320];
	struct userinfo_t userinfo;

	input("\nEnter an existing ig username for test: ", username, 320);
	input("Enter the correct password: ", password, 320);

	int r = gen_basic_userinfo_file_config(
		"./userinfo.conf", username, password, curl
	);
	int r2 = get_userinfo_from_file_config("./userinfo.conf", &userinfo);	

	if (!r || !r2) return 0;

	printf("\n!!! ATTENTION !!!\n");
	printf("Review manually the generated ./userinfo.conf file \n");
	printf("and make sure there is valid information\n\n");
	curl_easy_cleanup(curl);
	return 1;
}
