#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../include/utils.h"
#include "../include/urls.h"
#include "../include/selfuser-info.h"
#include "../include/screens.h"
#include "../include/one-command-line.h"

int main(int argc, char **argv)
{
	struct userinfo_t userinfo;
	CURL *curl = curl_easy_init();

	// Cli one command line mode
	if (argc > 1) {
		if (!(one_command_action(argc, argv))) {
			printf("Bad usage. -h or --help to display help.\n");
			return 1;
		}
		
		return 0;
	}

	// Interactive mode
	main_screen(&userinfo);

	curl_easy_cleanup(curl);

	return 0;
}
