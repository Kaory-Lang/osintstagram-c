#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>

#include "./include/features.test.h"
#include "../include/features.h"
#include "../include/selfuser-info.h"

int user_from_id_test() {
	CURL *curl = curl_easy_init();
	struct userinfo_t userinfo;
	int r = get_userinfo_from_file_config("./userinfo.conf", &userinfo);

	if (!r) return 0;

	char *value = user_from_id("25025320", curl, &userinfo);
	curl_easy_cleanup(curl);
	if (value == NULL) return 0;

	int result = strcmp(value, "instagram");
	free(value); value = NULL;

	return result == 0 ? 1 : 0;
}
