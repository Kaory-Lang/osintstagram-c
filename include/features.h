#ifndef FEATURES_H_
#define FEATURES_H_

#include "./selfuser-info.h"

/* Pass an IG user id and get the corresponding username for the account
 *
 * char * -> Pointer to the IG username str. Need to be freed
 * ---
 * char * <- String of the IG profile id
 * CURL * <- Curl pointer
 * struct userinfo_t * <- Pointer of the selfuser_info struct
 * */
char *user_from_id(char *id, CURL *curl, struct userinfo_t *userinfo);

#endif // FEATURES_H_
